Make your ant script download Ivy automatically.
Just paste the following lines in your `build.xml`:

```
<!-- download and load Ivy -->
<import file="bootstrap-ivy.xml" optional="true"/>
<target name="-bootstrap-get" unless="ivy.jar.file">
    <get usetimestamp="true"
         src="https://bitbucket.org/cogumbreiro/bootstrap-ivy/raw/master/bootstrap-ivy.xml"
         dest="bootstrap-ivy.xml" />
</target>
<target name="-ivy-init" depends="-bootstrap-get">
    <antcall target="-ivy-load" />
</target>
```

Add a dependency to target `-ivy-init` to ensure it is loaded. For example:

```
<project name="project" xmlns:ivy="antlib:org.apache.ivy.ant">
    <!-- download and load Ivy -->
    <import file="bootstrap-ivy.xml" optional="true"/>
    <target name="-bootstrap-get" unless="ivy.jar.file">
        <get usetimestamp="true"
             src="https://bitbucket.org/cogumbreiro/bootstrap-ivy/raw/master/bootstrap-ivy.xml"
             dest="bootstrap-ivy.xml" />
    </target>
    <target name="-ivy-init" depends="-bootstrap-get">
        <antcall target="-ivy-load" />
    </target>
    <!-- start Ivy in the initialization target: -->
    <target init="init" depends="-ivy-init">
       ...
    </target>
    ...
</project>
```
